  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ URL::asset('adminlte/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Ryan Vergel Hojilla</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="/">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pencil-square-o"></i>
            <span>Inventory</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ URL::route('inventory.list') }}"><i class="fa fa-circle-o"></i> View Inventory</a></li>
            <li><a href="{{ URL::route('inventory.create') }}"><i class="fa fa-circle-o"></i> Create Inventory</a></li>
            <li><a href="{{ URL::asset('adminlte/pages/charts/chartjs.html') }}"><i class="fa fa-circle-o"></i> Inventory History</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-opencart"></i>
            <span>Orders</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ URL::asset('adminlte/pages/charts/inline.html') }}"><i class="fa fa-circle-o"></i> Order List</a></li>
            <li><a href="{{ URL::asset('adminlte/pages/charts/inline.html') }}"><i class="fa fa-circle-o"></i> Add Orders</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-sitemap"></i>
            <span>Products</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ URL::route('products.list') }}"><i class="fa fa-circle-o"></i> Product List</a></li>
            <li><a href="{{ URL::route('products.create') }}"><i class="fa fa-circle-o"></i> Add Product</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Suppliers</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ URL::asset('adminlte/pages/charts/inline.html') }}"><i class="fa fa-circle-o"></i> Supplier List</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-line-chart"></i>
            <span>Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ URL::asset('adminlte/pages/charts/inline.html') }}"><i class="fa fa-circle-o"></i>Beginning Inventory Report</a></li>
            <li><a href="{{ URL::asset('adminlte/pages/charts/inline.html') }}"><i class="fa fa-circle-o"></i>Closing Inventory Report</a></li>
          </ul>
        </li>
        <li class="header hidden">LABELS</li>
    </section>
    <!-- /.sidebar -->
  </aside>