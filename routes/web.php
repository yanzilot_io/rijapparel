<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->name('/');

Auth::routes();

//Inventory Routes
Route::get('/inventory', 'CreateInventoryController@index')->name('inventory');
Route::get('/inventory/list', 'InventoryController@index')->name('inventory/list');
Route::get('/inventory/create', 'InventoryController@create')->name('inventory/create');

Route::group(['prefix' => 'inventory'], function () {
	Route::get('list',[
		'as' => 'inventory.list',
		'uses' => 'InventoryController@index'
	]);
	Route::get('create',[
		'as' => 'inventory.create',
		'uses' => 'InventoryController@create'
	]);
	Route::get('show/{id}',[
		'as' => 'inventory.show',
		'uses' => 'InventoryController@show'
	]);
	Route::post('store',[
		'as' => 'inventory.store',
		'uses' => 'InventoryController@store'
	]);
	Route::post('update/{id}',[
		'as' => 'inventory.update',
		'uses' => 'InventoryController@update'
	]);
});



//Products route
Route::group(['prefix' => 'products'], function () {
	Route::get('list',[
		'as' => 'products.list',
		'uses' => 'ProductsController@index'
	]);
	Route::get('paginate',[
		'as' => 'products.paginate',
		'uses' => 'ProductsController@paginate'
	]);
	Route::get('create',[
		'as' => 'products.create',
		'uses' => 'ProductsController@create'
	]);
	Route::get('show/{id}',[
		'as' => 'products.show',
		'uses' => 'ProductsController@show'
	]);
	// Route::post('store',[
	// 	'as' => 'products.store',
	// 	'uses' => 'ProductsController@store'
	// ]);
	Route::post('update/{id}',[
		'as' => 'products.update',
		'uses' => 'ProductsController@update'
	]);
});
