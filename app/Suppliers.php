<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suppliers extends Model
{
    protected $fillable = [
    				'name',
    				'company_name',
    				'address',
    				'contact_no',
    				'is_active'
				];
}
