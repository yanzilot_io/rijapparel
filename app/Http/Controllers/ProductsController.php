<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Products;
use \DB;
use \View;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Products::getProductList();

        return View::make('products.list')
                    ->with('products', $products);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function paginate()
    {
        $products = Products::getProductList();

        return $products;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $prod_type = array_map('intval', explode(" ", $request->product_type));
        $supp = array_map('intval', explode(" ", $request->supplier));
        $products = new Products;
            $products = [
                'sku' => $request->sku,
                'product_name' => $request->product_name,
                'description' => $request->description,
                'product_type' => $prod_type[0],
                'quantity' => $request->quantity,
                'store_price' => $request->store_price,
                'supplier_id' => $supp[0],
                'selling_price' => $request->store_price,
                'crit_rate' => $request->crit_rate,
                'reorder_point' => $request->reorder_point,
                'image' => $request->image
            ];

            DB::table('products')->insert($products);

        return view('products.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
