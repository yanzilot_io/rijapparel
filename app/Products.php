<?php

namespace App;

use \DB;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [ 
					 'sku', 
					 'product_name', 
					 'description', 
					 'product_type', 
					 'quantity',
					 'store_price',
					 'selling_price',
					 'reorder_point',
					 'supplier',
					 'crit_rate',
					 'image'
				];


    public static function getProductList() {
        $products = self::all();

        return $products;
    }
}
